#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh rmjar.sh"
	exit 1
}


# copy jar
echo "begin rm ruoyi-gateway "
rm -rf ./ruoyi/gateway/jar/ruoyi-gateway.jar

echo "begin rm ruoyi-auth "
rm -rf ./ruoyi/auth/jar/ruoyi-auth.jar

echo "begin rm ruoyi-visual "
rm -rf ./ruoyi/visual/monitor/jar/ruoyi-visual-monitor.jar

echo "begin rm ruoyi-modules-system "
rm -rf ./ruoyi/modules/system/jar/ruoyi-modules-system.jar

echo "begin rm ruoyi-modules-file "
rm -rf ./ruoyi/modules/file/jar/ruoyi-modules-file.jar

echo "begin rm ruoyi-modules-job "
rm -rf ./ruoyi/modules/job/jar/ruoyi-modules-job.jar 

echo "begin rm ruoyi-modules-gen "
rm -rf ./ruoyi/modules/gen/jar/ruoyi-modules-gen.jar 

